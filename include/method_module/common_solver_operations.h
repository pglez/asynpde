#include <structure_paralleltestbed.h>

double calcSD(double , double *, int ) ;

double calctime(void*,double );

double returnseed_(void *);

void initrngrandomserial_(void *);

double getrngrandomserial_(void *);

int chargedimension_(void *, int *);

int chargedimensionopenmp_(void *, int *);

double callfitnessfunction_(void *(*fitnessfunction)(double*, void *), void *, double *, int *, double *, double *);

double callfitnessfunctionfortran_(void *(*fitnessfunction)(double*, void *,double*), void *, double *, int *, double *, double *, double *);

double callfitnessfunctionopenmp_(void *(*fitnessfunction)(double*, void *), void *, double *, int *, double *, double *, int *);

void charge_DE_parameters(void *, int , int *, double *, double *);

void DE_correction_bounds(double *, int, double* , double* );

void generate_population_(void *, double *, int , int , void *(*fitnessfunction)(double*,void*), double*, double*);

void read_matrix_init_points(char const *, double *, int, int, int, int);

double sumvar(double *, int , int);

int allocate_QuickShort(double *, int , int, int);

double* reorderVector_QuickShort(double*, int, int, int);

void replaceWorst(double *, int, int,  double *, int);

void replaceWorstRecp(double *, int, int,  double *, int, double *, double *, int );

void replaceRandom(double *, int, int,  double *, int);

void returnBest(double *, int, int,  double *, int);

void returnBestTabuList(experiment_total , double *, int , int , double *, int , double *, double *, int );

void returnIndv(experiment_total, double *, int, int, double *, int, double *, double *, int );

void replaceIndv(experiment_total, double *, int, int, double *, int, double *, double *, int );

double* mutation_operation(void *, double *, double *, int , int, int, int, int, double, double);

int extract_best_index(double *, int, int);

int initializebenchmarks_(void *, double *, double *, int *) ;

int initializebenchmarksopenmp_(void *, double *, double *, int *, int *) ;

double calc_euclidean_distance(double *, double *, int , double *, double *);

void insert_matrix_tabu_list(double *, double *, int, int, int);

void converttonormal_(double *, int *);

void converttonormal2_(double *, int *, int *);

void converttolog_(double *, int *);

void converttolog2_(double *, int *, int *);

void chargeuseroptions_(void *, float *, int *, double *, double *,
        int *, int *, int *, int *, int *, int *, int *);

void chargeglobaloptions_( void *, int *, int *, int *, int *, int *, 
        char* , int *, double * , int *, double * , int *);

void chargelocaloptions_( void *, int *, int *, int *, int *, float * , 
        char * , int *, int *, int *, float *, 
        float *, int *, int *, char *, double *) ;

int chargeboundsnconst_(void *, double *, double *, int * , double *, double *, int *);


int chargebounds_(void *, double *, double *, int *);


int allocatematrixdouble_(double *, int *, int *);

void setinfinity_(double*);

void setnan_(double*);

int chargeproblemargs_(void *, int *, int *, int *);

void generatepopulation_(void* , double *, int , int , void *(*fitnessfunction)(double*, void *), double *, double *, long *);

void reorder_best(double *, int , int );


int extract_worst_index(double *, int , int );

int getbench_(void *);


double gettotaltime_( result_solver *);

double getparalleltime_( result_solver *);

double getlocalsolvertime_(  result_solver *);

int getidp_(void *);

int ishete_(void *);

int iscoop_(void *);

void setiteration_(void *, int *);

void setnumit_(void *, int *);