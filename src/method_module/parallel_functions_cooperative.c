#ifdef MPI2 


#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <float.h>
#include <limits.h>
#include <structure_paralleltestbed.h>
#include <common_solver_operations.h>
#include <parallel_functions.h>
#include <configuration.h>
#include <mpi.h>
#include <dynamic_list.h>
#include <def_errors.h>
#include <string.h>


void setnproc_(void *exp, int *NPROC){
    
    experiment_total *exp1;
    
    exp1 = (experiment_total *) exp;
    *NPROC = exp1[0].par_st->NPROC;;
}


void createcooperativetopology_(void *exp){
    
    experiment_total *exp1;
    
    exp1 = (experiment_total *) exp;
    createtopology_(exp1);
    MPI_Comm_rank(exp1[0].execution.topology.comunicator, &(exp1[0].execution.idp));
    
    
}


void createcooperativestructs_(void *exp, int *D){
    int i,j;
    experiment_total *exp1;
    exp1 = (experiment_total *) exp;
    
    create_list( &(exp1[0].execution.l), exp1[0].execution.NM, *D+1 );
    exp1[0].execution.state = (state_buffers_procs *) malloc(exp1[0].execution.NPROC*sizeof(state_buffers_procs));
    
    for (i=0;i<exp1[0].execution.NPROC;i++) {
        exp1[0].execution.state[i].num_send_buffers = 0;
        exp1[0].execution.state[i].num_recv_buffers = 0;
        exp1[0].execution.state[i].num_send_buffers_other=0;
        exp1[0].execution.state[i].recv_active=0;
    }

    exp1[0].execution.tabusend.size = 40;
    exp1[0].execution.tabusend.counter = 0;
    exp1[0].execution.tabusend.array = (double *) malloc(exp1[0].execution.tabusend.size * (*D + 1)* sizeof (double));

    for (i = 0; i < exp1[0].execution.tabusend.size; i++) {
        for (j = 0; j < (*D + 1); j++) exp1[0].execution.tabusend.array[i * (*D + 1) + j] = DBL_MAX;
    }
}



void cooperativedist_(void *exp, double *matrix, int *D, double *vector) { 
    experiment_total *exp1;
    exp1 = (experiment_total *) exp;
    
    MPI_Scatter(matrix, exp1[0].execution.tam * (*D + 1), MPI_DOUBLE, vector, exp1[0].execution.tam * (*D + 1), MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
}



void cooperativedistelement_(void *exp, double *matrix, int *size, double *vector) { 
    experiment_total *exp1;
    exp1 = (experiment_total *) exp;
    
    MPI_Scatter(matrix, *size, MPI_DOUBLE, vector, *size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
}

void cooperativebcastelement_(void *exp, double *matrix, int *size, int *idp) { 
    experiment_total *exp1;
    exp1 = (experiment_total *) exp;
   
    MPI_Bcast(matrix, *size, MPI_DOUBLE, *idp, MPI_COMM_WORLD);
}


void cooperativebcastelementint_(void *exp, int *matrix, int *size) { 
    experiment_total *exp1;
    exp1 = (experiment_total *) exp;
    
    MPI_Bcast(matrix, *size, MPI_INT,  0, MPI_COMM_WORLD);
}


void returnmaxelementint_(void *exp, int *value, int *valuemax) { 
    experiment_total *exp1;
    exp1 = (experiment_total *) exp;
    
    MPI_Reduce(value, valuemax, 1,  MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
}

void returnminelementint_(void *exp, int *value, int *valuemax) { 
    experiment_total *exp1;
    exp1 = (experiment_total *) exp;
    
    MPI_Reduce(value, valuemax, 1,  MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
}

void returnminelement_(void *exp, double *value, double *valuemax) { 
    experiment_total *exp1;
    exp1 = (experiment_total *) exp;
    
    MPI_Reduce(value, valuemax, 1,  MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
}

void returnminlocelement_(void *exp, double *value, double *valuemax, int *loc, int *idp) { 
    experiment_total *exp1;
    exp1 = (experiment_total *) exp;
    int i;
    double *vals, aux;
    
    MPI_Barrier(MPI_COMM_WORLD);
    vals = (double *) malloc( exp1[0].execution.NPROC * sizeof(double));
    
    MPI_Gather(value, 1 , MPI_DOUBLE, vals, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(vals, exp1[0].execution.NPROC,MPI_DOUBLE, 0 , MPI_COMM_WORLD);
    
    aux = DBL_MAX;
    for (i=0;i<exp1[0].execution.NPROC;i++) {
        if (vals[i]<aux) {
            aux = vals[i];
            *loc = i;
        }
    }
    *valuemax = aux;
    free(vals);
    vals = NULL;
}

int destroycooperativestructures_(void *exp) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp;
    int i;
    
    if (exp1[0].execution.topology.left != NULL) {
        free(exp1[0].execution.topology.left);
        exp1[0].execution.topology.left = NULL;
    }
    
    if (exp1[0].execution.topology.rigth != NULL) {
        free(exp1[0].execution.topology.rigth);
        exp1[0].execution.topology.rigth = NULL;
    }
    
    
    if (exp1[0].execution.st != NULL) {
        free(exp1[0].execution.st);
        exp1[0].execution.st = NULL;
    }
      
    destroy_list(&(exp1[0].execution.l));

    
    free(exp1[0].execution.state);
    exp1[0].execution.state=NULL;
    
    
    free( exp1[0].execution.tabusend.array);
     exp1[0].execution.tabusend.array=NULL;
     
    return 1;
}



void initcooperativestoppingcriteria_( void *exp) {
    int i;
    experiment_total *exp1;
    exp1 = (experiment_total *) exp;
    int modulo;

    exp1[0].execution.st = (int *) malloc(exp1[0].execution.NPROC * sizeof (int));
    exp1[0].execution.request_recept = (MPI_Request *) malloc(exp1[0].execution.NPROC * sizeof (MPI_Request));

    modulo = exp1[0].execution.rep+100;    
    for (i = 0; i < exp1[0].execution.NPROC; i++) {
            MPI_Irecv(&(exp1[0].execution.st[i]), 1, MPI_INT,
                i, modulo, MPI_COMM_WORLD, &(exp1[0].execution.request_recept[i]));
    }
}


double * initsendbuffer_(void *exp, int *D) {
    experiment_total *exp1;    
    exp1 = (experiment_total *) exp;
    double *sendB;
    
    
    exp1[0].execution.size_send_buffer = 100;
    exp1[0].execution.send_id = 0;
    sendB = (double *) malloc(exp1[0].execution.size_send_buffer * exp1[0].execution.NM * (*D+1) * sizeof(double) );
    
    return sendB;
}


double * returnssendbuffer_(void *exp,  int *D, double *sendB) {
    experiment_total *exp1;    
    int id;
    exp1 = (experiment_total *) exp;

    id = exp1[0].execution.send_id;
    exp1[0].execution.send_id = exp1[0].execution.send_id + 1;
    if (exp1[0].execution.send_id > exp1[0].execution.size_send_buffer) {
        
        exp1[0].execution.send_id = 0;
    }
    
    return &sendB[id*exp1[0].execution.NM*(*D+1)];
}

void destroysendbuffer_(void *vect) {
    if (vect != NULL) {
        free(vect);
        vect = NULL;
    }
}


int checkcooperativemigrationcriteria_(void *exp) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp;
    
        if ( exp1[0].execution.NPROC != 1 ) {
            exp1[0].execution.contadorMigra= exp1[0].execution.contadorMigra+1;
            if ( exp1[0].execution.num_it > 0 ) {
            	if ( exp1[0].execution.contadorMigra >=  exp1[0].execution.num_it) {
                   exp1[0].execution.enterMigrat = 1;
                   exp1[0].execution.contadorMigra = 0;
                   return 1;
            	}
	    }
        }
    
}



void migrationcooperativesent_(void *exp, double *populnew, int *stop, int *D, double *LB, double *UB, int *rest) {
    experiment_total *exp1;
    int t, i, flag;
    positionL *p1, *p2;
    double *sendV, *recV;
    MPI_Request *reqM, reqS, reqS2;
    MPI_Request *request_num_recv_buffers;
    exp1 = (experiment_total *) exp;
   
    int *num_send_buffers, *num_recv_buffers, *num_send_buffers_other;
      
    
     
// ENVIAMOS AS SOLUCIONS PERTINENTES 
    if (exp1[0].execution.enterMigrat == 1 && *stop < 1) {
        exp1[0].execution.enterMigrat = 0;
 
        sendV = (double *) malloc(exp1[0].execution.NM * (*D + 1) * sizeof (double));
 	returnIndv(*exp1, populnew, (exp1[0].execution.tam), *D, sendV, exp1[0].execution.NM, UB, LB, *rest);

        for (t = 0; t < exp1[0].execution.topology.num_r; t++) {
           // num_send_buffers = &(exp1[0].execution.state[exp1[0].execution.topology.rigth[t]].num_send_buffers);
           // *num_send_buffers = *num_send_buffers + 1;
           // MPI_Isend(num_send_buffers, 1, MPI_INT,
           //         exp1[0].execution.topology.rigth[t], exp1[0].execution.rep+1,
           //         exp1[0].execution.topology.comunicator, &reqS2);     
            
	   // 	    returnIndv(*exp1, populnew, (exp1[0].execution.tam), *D, sendV, exp1[0].execution.NM, UB, LB, *rest);       
            
          //  printf("id %d in iteration %d sent to %d - %lf\n",exp1[0].execution.idp, exp1[0].execution.iteration, exp1[0].execution.topology.rigth[t], sendV[*D]);
	    MPI_Isend(sendV, (exp1[0].execution.NM) * (*D + 1), MPI_DOUBLE,
                    exp1[0].execution.topology.rigth[t], exp1[0].execution.rep+1,
                    MPI_COMM_WORLD, &reqS);
        }

        
        
    	for (t = 0; t < exp1[0].execution.topology.num_left; t++) {
           // num_send_buffers_other = &(exp1[0].execution.state[exp1[0].execution.topology.left[t]].num_send_buffers_other);
           // num_recv_buffers = &(exp1[0].execution.state[exp1[0].execution.topology.left[t]].num_recv_buffers);
           // request_num_recv_buffers = &(exp1[0].execution.state[exp1[0].execution.topology.left[t]].request_num_recv_buffers);
            
            
            
           // flag=0;
           // if (exp1[0].execution.state[exp1[0].execution.topology.left[t]].recv_active == 0) {

          //      MPI_Irecv(num_send_buffers_other, 1, MPI_INT, exp1[0].execution.topology.left[t], exp1[0].execution.rep+1, exp1[0].execution.topology.comunicator, 
          //          &(exp1[0].execution.state[exp1[0].execution.topology.left[t]].request_num_recv_buffers));
          //      exp1[0].execution.state[exp1[0].execution.topology.left[t]].recv_active = 1;
                
           //     MPI_Test( &(exp1[0].execution.state[exp1[0].execution.topology.left[t]].request_num_recv_buffers), &flag, MPI_STATUS_IGNORE);
           // } else {
           //     MPI_Test( &(exp1[0].execution.state[exp1[0].execution.topology.left[t]].request_num_recv_buffers), &flag, MPI_STATUS_IGNORE);
           // }
            
            
            //if (flag == 1) {
                
             //   exp1[0].execution.state[exp1[0].execution.topology.left[t]].recv_active = 0;
               //     for (i=0;i<*num_send_buffers_other-*num_recv_buffers;i++) {
                        p1 = (exp1[0].execution.l)->end;
                        exp1[0].execution.migration = exp1[0].execution.migration + 1;
                        add_node(&((*exp1).execution.l), p1, exp1[0].execution.migration);
                        MPI_Irecv( return_reception_node(&((*exp1).execution.l), p1), (exp1[0].execution.NM) * (*D + 1), MPI_DOUBLE, exp1[0].execution.topology.left[t],
                            exp1[0].execution.rep+1, exp1[0].execution.topology.comunicator, get_request_node(&((*exp1).execution.l), p1));
                        
                        exp1[0].execution.migra_asin_wait = exp1[0].execution.migra_asin_wait + 1;
//                        *num_recv_buffers = *num_recv_buffers + 1;
              //      }
            //} 
            
       }
    }
        
}

void migrationsynchcooperativesent_(void *exp, int *D, double *populLocal, double *best, double *bestOLD, double *LB, double *UB) {
    experiment_total *exp1;
    double *bestInv, *bestInv2, *bestInvOld;
    int j, t, index, rest;
    MPI_Status status1, status2;

    rest = 0;
    exp1 = (experiment_total *) exp;

    if (exp1[0].execution.NPROC != 1) {
        bestInv = (double *) malloc(exp1[0].execution.NM * (*D + 1) * sizeof (double));
        bestInv2 = (double *) malloc(exp1[0].execution.NM * (*D + 1) * sizeof (double));
        bestInvOld = (double *) malloc(exp1[0].execution.NM * (*D + 1) * sizeof (double));
        exp1[0].execution.migration = exp1[0].execution.migration + 1;
        returnIndv(*exp1, populLocal, (exp1[0].execution.tam), *D, bestInv, exp1[0].execution.NM, UB, LB, rest);

        for (j = 0; j < exp1[0].execution.NM; j++) {
            memmove(&bestInvOld[j * (*D + 1)], & bestInv[j * (*D + 1)], (*D+1)*sizeof(double));
        }


        if (exp1[0].execution.idp != 0) {
            for (t = 0; t < exp1[0].execution.topology.num_left; t++) {
                MPI_Recv(bestInv, exp1[0].execution.NM * (*D + 1), MPI_DOUBLE, exp1[0].execution.topology.left[t], exp1[0].execution.rep+1, exp1[0].execution.topology.comunicator, &status1);
                replaceIndv(*exp1, populLocal, (exp1[0].execution.tam), *D, bestInv, exp1[0].execution.NM, LB, UB, rest);
            }
        }

        for (t = 0; t < exp1[0].execution.topology.num_r; t++) {
            MPI_Send(bestInvOld, exp1[0].execution.NM * (*D + 1), MPI_DOUBLE, exp1[0].execution.topology.rigth[t], exp1[0].execution.rep+1, exp1[0].execution.topology.comunicator);
        }

        if (exp1[0].execution.idp == 0) {
            for (t = 0; t < exp1[0].execution.topology.num_left; t++) {
                MPI_Recv(bestInv2, exp1[0].execution.NM * (*D + 1), MPI_DOUBLE, exp1[0].execution.topology.left[t], exp1[0].execution.rep+1, exp1[0].execution.topology.comunicator, &status2);
                replaceIndv(*exp1, populLocal, (exp1[0].execution.tam), *D, bestInv2, exp1[0].execution.NM, LB, UB,rest);
            }
        }


        exp1[0].execution.enterMigrat = 0;

        index = extract_best_index(populLocal, exp1[0].execution.tam, *D);
        *best = populLocal[index * (*D + 1) + *D];
        
	if (*best < *bestOLD) {
            *bestOLD = *best;
            exp1[0].execution.stuckcount = 0;
        }
        
        if (bestInv != NULL) {
            free(bestInv);
            bestInv = NULL;        
        }
        
        if (bestInvOld != NULL) {
            free(bestInvOld);
            bestInvOld = NULL;
        }
        
        if (bestInv2 != NULL) {
            free(bestInv2);
            bestInv2 = NULL;
        }        
        
    }
}

void migrationcooperativereception_( void *exp, void*(*fitnessfunction)(double*, void *),double *populnew, int *stop, int *D, double *Xl, double *Xm, int *rest) {
    experiment_total *exp1;

    int p1_int, p2_int;
    double *bestInv2;
    MPI_Request *reqM;
    int flag;
    int i,j;
    int continua_check;
    positionL *p1;
    int update1;
     
    update1 =0;
    exp1 = (experiment_total *) exp;
        // MIGRATION RECEPTION
        if (exp1[0].execution.migra_asin_wait > 0 && *stop < 1) {
            p1 = (exp1[0].execution.l)->init;
            p1_int = 0;
            p2_int = exp1[0].execution.l->length;
            continua_check = 1;
            update1 = 0;
            flag = 0;
	    //printf("%d - while1 \n",exp1[0].execution.idp);
            while (p1_int < p2_int && continua_check == 1 ) {
                reqM = get_request_node(&(exp1[0].execution.l), p1);
                MPI_Test( reqM, &flag, MPI_STATUS_IGNORE);
                    
                if (flag == 1 && ((struct nodeL *) p1)->next->element->completado == 0) {
                    
                    ((struct nodeL *) p1)->next->element->completado = 1;
                    bestInv2=NULL;
                    bestInv2 = return_reception_node(&(exp1[0].execution.l), p1);
                   
                    for (i = 0; i < exp1[0].execution.NM; i++) {
			//printf("RECV %d recv %lf in iteration %d\n",exp1[0].execution.idp, bestInv2[i * (*D + 1) + *D], exp1[0].execution.iteration);
                        if (*stop < 1) {
                            if (bestInv2[i * (*D + 1) + *D] < exp1[0].execution.ftarget) {
                                callfitnessfunction_(fitnessfunction, (void *) exp1,   &(bestInv2[i * (*D + 1)]),  D, Xl, Xm);
				printf(" (NEW => %lf \n)", bestInv2[i * (*D + 1) + *D]);
                            }
			}
                    }
                
                    replaceIndv(*exp1, populnew, exp1[0].execution.tam, *D, bestInv2, exp1[0].execution.NM, Xl, Xm, *rest);
                    flag = 1;
                    exp1[0].execution.migra_asin_wait= exp1[0].execution.migra_asin_wait-1;
                    continua_check = 1;
                    update1 = 1;
                } else {
                    continua_check = 0;
                }
                p1 = (positionL *) ((struct nodeL *) p1)->next;
                p1_int++;
          }
            
            
      
            if (update1 == 1) {              
                update_list(&(exp1[0].execution.l));
                exp1[0].execution.update = 0;
            }
            
        }
}



void asynchronousstoppingcriteriawithrestart_(void *exp, int *stop, long *evaluation_local, double *best,double *matrix,int *D) {
    experiment_total *exp1;
    int i;
    MPI_Request mpir;
    int modulo;
    exp1 = (experiment_total *) exp;
    
               
        if (*stop < 1) {
            if ( *evaluation_local * exp1[0].execution.NPROC >= exp1[0].execution.maxfunevals) {
                *stop = 2;
                if (*stop < 1) {
                    *stop = 1;
                    for (i = 0; i < exp1[0].execution.NPROC; i++) {
                        if (exp1[0].execution.idp != i) {
                            exp1[0].execution.st_sent = 1;
                            modulo = exp1[0].execution.rep+100;
                            MPI_Isend(&(exp1[0].execution.st_sent), 1, MPI_INT, i, modulo, MPI_COMM_WORLD, &mpir);
                        }
                    }
                }

            } else if ((*best < exp1[0].execution.ftarget)
                    || (exp1[0].execution.stuckcount > exp1[0].execution.stuckcond_lowVar && 
                    (sumvar(matrix, exp1[0].execution.tam, *D) / *D) < exp1[0].execution.minvarcondition)
                    ) {
                if (*stop < 1) {
                    *stop = 1;
                    for (i = 0; i < exp1[0].execution.NPROC; i++) {
                        if (exp1[0].execution.idp != i) {
                            exp1[0].execution.st_sent = 1;
			    modulo = exp1[0].execution.rep+100;
                            MPI_Isend(&(exp1[0].execution.st_sent), 1, MPI_INT, i, modulo, MPI_COMM_WORLD, &mpir);
                        }
                    }
                }

            }
        }
}


void asynchronousstoppingcriteria_(void *exp, int *stop, long *evaluation_local, double *best, double *cputime, double *maxtime, int *stopOptimization) {
    experiment_total *exp1;
    int i;
    MPI_Request mpir;
    int modulo;
    exp1 = (experiment_total *) exp;
    

    if (*stop < 1) {

        if (*evaluation_local * exp1[0].execution.NPROC >= exp1[0].execution.maxfunevals) {
            *stop = 1; 
        }
        else if (*cputime >= *maxtime) {
            *stop = 2; 
        } else if (*stopOptimization == 1) {
            *stop = 4; 
        } else if (*best < exp1[0].execution.ftarget) {
            *stop = 3;
        }
     
        if ((*stop == 4) || (*stop == 3)) {
                for (i = 0; i < exp1[0].execution.NPROC; i++) {
                    //if (exp1[0].execution.idp != i) {
                        exp1[0].execution.st_sent = 1;
			modulo = exp1[0].execution.rep+100;
                        MPI_Isend(&(exp1[0].execution.st_sent), 1, MPI_INT, i, modulo, MPI_COMM_WORLD, &mpir);
                    //}
                }
        }
        
    }
}


void synchronousstoppingcriteriawithrestart_(void *exp, int *stop, long *evaluation_local, double *best, double *matrix, int *D) {
    experiment_total *exp1;
    int i;
    int modulo; 
    exp1 = (experiment_total *) exp;


    if (*stop < 1) {
        if (*evaluation_local * exp1[0].execution.NPROC >= exp1[0].execution.maxfunevals) {
            *stop = 2;
        } else if ((*best < exp1[0].execution.ftarget)
                || (exp1[0].execution.stuckcount > exp1[0].execution.stuckcond_lowVar &&
                (sumvar(matrix, exp1[0].execution.tam, *D) / *D) < exp1[0].execution.minvarcondition)
                ) {
            if (*stop < 1) {
                *stop = 1;
                for (i = 0; i < exp1[0].execution.NPROC; i++) {
                    if (exp1[0].execution.idp != i) {
                        exp1[0].execution.st_sent = 1;
			modulo = exp1[0].execution.rep+100;
                        MPI_Send(&(exp1[0].execution.st_sent), 1, MPI_INT, i, modulo, MPI_COMM_WORLD);
                    }
                }
            }

        }
    }

}

void setcountstopsvar_(void *exp, int *stop, int *contador) {
    experiment_total *exp1;

    exp1 = (experiment_total *) exp;
    
    if (exp1[0].execution.NPROC != 1) {
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Reduce(stop, contador, 1, MPI_INT, MPI_SUM, 0, exp1[0].execution.topology.comunicator);
        MPI_Bcast(contador, 1, MPI_INT, 0, exp1[0].execution.topology.comunicator);
    } else {
        if (*stop > 0)
            *contador = 1;
    }
}

int cooperativempitest_(void *exp, int *l) {
    int flag;
    experiment_total *exp1;
    
    exp1 = (experiment_total *) exp;
    flag = 0;
    MPI_Test(&(exp1[0].execution.request_recept[*l]), &flag, MPI_STATUS_IGNORE);
    return flag;
}





void gatherresults_(void *exp, double *matrixlocal, double *matrix, int *D, double *starttime, double *time_total, long *evaluation_local, long *eval_total) {
    experiment_total *exp1;
    
    exp1 = (experiment_total *) exp;  
    double endtime;
    double localtotaltime;
    int l,flag;

    for (l = 0; l < exp1[0].execution.NPROC; l++) {
        MPI_Test( &(exp1[0].execution.request_recept[l]), &flag, MPI_STATUS_IGNORE);
        if (flag == 0) {
            MPI_Cancel( &(exp1[0].execution.request_recept[l]));
            MPI_Wait(&(exp1[0].execution.request_recept[l]), MPI_STATUS_IGNORE);
        }
    }    
    
    if (exp1[0].execution.request_recept != NULL) {
        free(exp1[0].execution.request_recept);
        exp1[0].execution.request_recept = NULL;
    }
    
    
    MPI_Gather(matrixlocal, exp1[0].execution.tam * (*D + 1), MPI_DOUBLE, matrix, exp1[0].execution.tam * (*D + 1), MPI_DOUBLE, 0, exp1[0].execution.topology.comunicator);
    endtime = MPI_Wtime();
    localtotaltime = endtime - *starttime;
    MPI_Reduce(&localtotaltime, time_total, 1, MPI_DOUBLE, MPI_MAX, 0, exp1[0].execution.topology.comunicator);
    MPI_Reduce(evaluation_local, eval_total, 1, MPI_LONG, MPI_SUM, 0, exp1[0].execution.topology.comunicator);
    MPI_Bcast(time_total,1,MPI_DOUBLE,0,exp1[0].execution.topology.comunicator);
    MPI_Bcast(eval_total,1,MPI_LONG, 0,exp1[0].execution.topology.comunicator);
}


void gatherresultsserialize_(void *exp, void *local_s, double *smatrixlocal, double *smatrix, int *sizetotal, double *time1, double *time_total, 
        long *evaluation_local, long *eval_total) {
    experiment_total *exp1;
    local_solver *local1;
    int totallocal;
    int l,flag;
    exp1 = (experiment_total *) exp;  
    local1 = (local_solver *) local_s;
    totallocal = 0;
    
    for (l = 0; l < exp1[0].execution.NPROC; l++) {
        MPI_Test( &(exp1[0].execution.request_recept[l]), &flag, MPI_STATUS_IGNORE);
        if (flag == 0) {
            MPI_Cancel( &(exp1[0].execution.request_recept[l]));
            MPI_Wait(&(exp1[0].execution.request_recept[l]), MPI_STATUS_IGNORE);
            
        }
    }    
    
    if (exp1[0].execution.request_recept != NULL) {
        free(exp1[0].execution.request_recept);
        exp1[0].execution.request_recept = NULL;
    }
    
    
    MPI_Gather(smatrixlocal, *sizetotal, MPI_DOUBLE, smatrix, *sizetotal, MPI_DOUBLE, 0, exp1[0].execution.topology.comunicator);
    MPI_Bcast(smatrix, *sizetotal*exp1->execution.NPROC, MPI_DOUBLE, 0,exp1[0].execution.topology.comunicator);
    MPI_Reduce(time1, time_total, 1, MPI_DOUBLE, MPI_MAX, 0, exp1[0].execution.topology.comunicator);
    MPI_Reduce(evaluation_local, eval_total, 1, MPI_LONG, MPI_SUM, 0, exp1[0].execution.topology.comunicator);
    MPI_Reduce(&(local1->counter), &totallocal, 1, MPI_INT, MPI_SUM, 0, exp1[0].execution.topology.comunicator);
    local1->counter = totallocal;
    MPI_Bcast(time_total,1,MPI_DOUBLE,0,exp1[0].execution.topology.comunicator);
    MPI_Bcast(eval_total,1,MPI_LONG, 0,exp1[0].execution.topology.comunicator);
    MPI_Bcast(&(local1->counter),1,MPI_INT, 0,exp1[0].execution.topology.comunicator);

}





void setexpexecutionstuckcount(void *exp, int *valor) {
    experiment_total *exp1;
    
    exp1 = (experiment_total *) exp;
    
    exp1[0].execution.stuckcount = *valor;
}


int getexpexecutionstuckcount(void *exp) {
    experiment_total *exp1;
    
    exp1 = (experiment_total *) exp;
    
    return exp1[0].execution.stuckcount;
}





#endif
