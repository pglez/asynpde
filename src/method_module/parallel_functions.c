#ifdef MPI2

#include <structure_paralleltestbed.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <def_errors.h>
#include <AMIGO_problem.h>
#include <float.h>
#include <string.h>

double initmpi_(){

    return (double) MPI_Wtime();
}

double calctimempi_(void *exp, double *starttime) {
    double current;
    double valor;
    experiment_total *exp1;

    valor = 0;
    exp1 = (experiment_total *) exp;

    current = (double) MPI_Wtime();
    valor = current - *starttime;



    return valor;
}


int returnmigrationsize_(void *exp) {
    experiment_total *exp1;
    
    exp1 = (experiment_total *) exp;
    
    return exp1[0].par_st->migration_size;
}




void charge_island_size(experiment_total *exp1, int *tam, int *NP, int *NM, int idp, int coop){
    int NPROC, size;

    NPROC = exp1[0].par_st->NPROC;

    if (coop == 1) {
        *tam = *NP;
        size = (int) floorl((double) (*tam / exp1[0].par_st->migration_size));
        if ((size > 1) && (*tam > size) ) {
            *NM = (int) floorl((double) (*tam / exp1[0].par_st->migration_size));
        } else {
            *NM = 1;
        }

        if (*tam < size) {
            if (idp == 0) {
                perror(error6);
            }
            exit(6);
        }

    }
    else if (coop == 0) {
        *tam = (int) ceil((  *NP  /  NPROC));
        size = (int) floorl((double) (*tam / exp1[0].par_st->migration_size));
        if ((size > 1) && (*tam > size) ) {
            *NM = (int) floorl((double) (*tam / exp1[0].par_st->migration_size));
        } else {
            *NM = 1;
        }
        if (*tam < size) {
            if (idp == 0) {
                perror(error6);
            }
            exit(6);
        }
    }
    
     exp1[0].execution.tam = *tam;    
     printf("SIZE per ISLAND --> %d\n",*tam);

}



void chargeid_(void *exp1_, int *idp ) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp1_;
    
    *idp = exp1[0].execution.idp;
}


void chargecooperativeparameters_(void *exp1_, int *NP, int *tam, int *idp, double *ftarget, long *maxfunevals, int *ccop ) {
    experiment_total *exp1;
    int tam1, NM1;
    exp1 = (experiment_total *) exp1_;
    
    exp1[0].execution.num_it = exp1[0].par_st->migration_freq_ite;
    exp1[0].execution.NPROC = exp1[0].par_st->NPROC;
    exp1[0].execution.st_sent = 0;
    exp1[0].execution.stuckcond_lowVar = (int) 30 * exp1[0].execution.NPROC ;
    exp1[0].execution.minvarcondition = 1e-10 / exp1[0].execution.NPROC ;
    exp1[0].execution.stuckcount = 0;
    exp1[0].execution.migra_asin_wait = 0;
    exp1[0].execution.contadorMigra = 0;
    exp1[0].execution.enterMigrat = 0;  
    exp1[0].execution.migration = 0;
    exp1[0].execution.ftarget = *ftarget;
    exp1[0].execution.maxfunevals = *maxfunevals;
    
    charge_island_size(exp1, &tam1, NP, &NM1, exp1[0].execution.idp, *ccop );
    
    *idp = exp1[0].execution.idp;
    exp1[0].execution.tam = tam1;
    *tam=tam1;
    exp1[0].execution.NM = NM1;
    exp1[0].execution.NP = *NP;

}

int test_stop (void *array, int *stop, int NPROC) {
    int flag, l;
    MPI_Request *request_recep = (MPI_Request *) array;
        if (*stop < 1) {
            for (l = 0; l < NPROC; l++) {
                MPI_Test(&request_recep[l], &flag, MPI_STATUS_IGNORE);
                if (flag == 1) {
                    *stop = 1;
                    break;
                }
            }
        }
    
    return *stop;
}


int createtopology_(void *exp_){
    int di[1], period[1],reorder,i;
    MPI_Comm ring;
    char const *ringw;
    char const *starw;
    experiment_total *exp;
    
    exp = (experiment_total *) exp_;
    ringw = "ring";
    starw = "star";

    if (strcmp(exp->par_st->topology, ringw) == 0) {
        exp->execution.topology.left = (int *) malloc(sizeof (int));
        exp->execution.topology.rigth = (int *) malloc(sizeof (int));
        exp->execution.topology.num_left = 1;
        exp->execution.topology.num_r = 1;
        di[0] = exp->par_st->NPROC;
        period[0] = 1;
        reorder = 1;
        MPI_Cart_create(MPI_COMM_WORLD, 1, di, period, reorder, &ring);
        exp->execution.topology.comunicator = ring;
        MPI_Cart_shift(exp->execution.topology.comunicator, 0, 1, exp->execution.topology.left, exp->execution.topology.rigth);
    }
    else if (strcmp(exp->par_st->topology, starw) == 0) {
        //MPI_Cart_create(MPI_COMM_WORLD, 1, di, period, reorder, &exp->execution.topology.comunicator);
        if (exp->execution.idp == 0) {
                exp->execution.topology.left =  (int *) malloc( (exp->par_st->NPROC - 1 )* sizeof (int));
                exp->execution.topology.rigth = (int *) malloc( (exp->par_st->NPROC - 1 )* sizeof (int));
                exp->execution.topology.num_left = exp->par_st->NPROC - 1;
                exp->execution.topology.num_r = exp->par_st->NPROC - 1;
                exp->execution.topology.comunicator = MPI_COMM_WORLD;    
                for (i=1;i<exp->par_st->NPROC;i++) {
                    exp->execution.topology.rigth[i-1] = i;
                    exp->execution.topology.left[i-1]  = i;
                }
        } 
        else {
                exp->execution.topology.left =  (int *) malloc(sizeof (int));
                exp->execution.topology.rigth = (int *) malloc(sizeof (int));
                exp->execution.topology.num_left = 1;
                exp->execution.topology.num_r = 1;
                exp->execution.topology.comunicator = MPI_COMM_WORLD;   
                exp->execution.topology.rigth[0] = 0; 
                exp->execution.topology.left[0]  = 0;                
        }
        
        
    }
    else {
        perror(error4);
        exit(4);
    }
    
    return 1;
}

int destroy_topology(topology_data *topology) {
    if (topology->left != NULL) {
        free(topology->left);
        topology->left = NULL;
    }
    
    if (topology->rigth != NULL) {
        free(topology->rigth);
        topology->rigth = NULL;
    }

    return 1;
}

void chargue_DE_parameters(experiment_total exp,  double *_F, double *_CR, int id) {
    int NPROC;
    
    char const *homo;
    char const *hete;
    
    homo= "homo";
    hete = "hete";
    
    NPROC = exp.par_st->NPROC;
    if (strcmp(exp.par_st->islandstrategy,homo)==0 || NPROC < 5) {
            *_CR = (double) exp.methodDE->_CR;
            *_F = (double) exp.methodDE->_F;
    }
    else if (strcmp(exp.par_st->islandstrategy,hete)==0 && NPROC >= 5){
        if ( id % 6 == 0) {
            *_CR = 0.9;
            *_F =  0.9;     
        }
        else if ( id % 6 == 1) {
            *_CR = 0.9;
            *_F =  0.7;             
        }
        else if ( id % 6 == 2) {
            *_CR = 0.7;
            *_F =  0.9;             
        }
        else if ( id % 6 == 3) {
            *_CR = 0.7;
            *_F =  0.7;             
        }
        else if ( id % 6 == 4) {
            *_CR = 0.2;
            *_F =  0.9;             
        }  
        else if ( id % 6 == 5) {
            *_CR = 0.2;
            *_F =  0.7;             
        }        
    }
    else {
        perror(error5);
        exit(5);
    }
}



void chargecooperativeparametersfortran_(void *exp1_, int *NP, int *tam, int *idp, double *ftarget, long *maxfunevals ) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp1_;
    
    exp1[0].execution.num_it = exp1[0].par_st->migration_freq_ite;
    exp1[0].execution.NPROC = exp1[0].par_st->NPROC;
    exp1[0].execution.st_sent = 0;
    exp1[0].execution.stuckcond_lowVar = (int) 30 * exp1[0].execution.NPROC ;
    exp1[0].execution.minvarcondition = 1e-10 / exp1[0].execution.NPROC ;
    exp1[0].execution.stuckcount = 0;
    exp1[0].execution.migra_asin_wait = 0;
    exp1[0].execution.contadorMigra = 0;
    exp1[0].execution.enterMigrat = 0;  
    exp1[0].execution.migration = 0;
    exp1[0].execution.ftarget = *ftarget;
    exp1[0].execution.maxfunevals = *maxfunevals;
    
    *idp = exp1[0].execution.idp;
    exp1[0].execution.tam = *NP;
    *tam = exp1[0].execution.tam;
    exp1[0].execution.NM = exp1[0].par_st->migration_size;
    exp1[0].execution.NP = *NP;

}
#endif
        
        
        
