void Init_Rng(unsigned int seed)
{
	int i;
        unsigned int estado[624];
	estado[0] = seed;

	for (i = 1; i < 624; i++) 
		estado[i] = 1812433253UL * ( estado[i - 1] ^ ( estado[i - 1] >> 30 ) ) + i;
	InitWELLRNG19937a(estado);
}

