#include <sersolverfortran.h>
#include <parallelsolverfortran.h>
#include <stdlib.h>
#include <string.h>
#include <structure_paralleltestbed.h>
#include <common_solver_operations.h>
#include <AMIGO_problem.h>
#include <configuration.h>
#include <amigoRHS.h>
#include <math.h>
#include <input_AMIGO.h>
#include <hdf5.h>
#include <input_module.h>

void*  functiontest(double *x,  void *data, double *g) {
       experiment_total *exp1;
       exp1 = (experiment_total *) data;
       AMIGO_problem* amigo_problem;
       amigo_problem = exp1[0].amigo;
       
	output_function *res;
        res = (output_function *) malloc(sizeof(output_function));
        int D,i;
        D = amigo_problem->nx;
        double s, p, fr;
        
        fr = 4000.0;
        s = 0;
        p=1;
        
        for (i = 0; i < D; i++) {
            s=s+pow( x[i] ,2);
        }
        for (i = 0; i < D; i++) {
            p=p*cos( x[i] /sqrt(i+1)); 
        }
                        
        
	//res = 4.0*x[0]*x[0]-2.1*pow(x[0],4)+(1.0/3.0)*pow(x[0],6)+x[0]*x[1]-4.0*x[1]*x[1]+4.0*pow(x[1],4);
        res->value = s/fr-p+1.0;
        
	return (void *) res;
}



int load_benchmark_test(experiment_total *exp, double *target, double *ftarget) {
    int D,i;
    double Up, Lo;
    
    
    D = (*exp).test.bench.dim;
    
    Up = exp->test.bench.max_dom[0]; 
    Lo = exp->test.bench.min_dom[0];
    
    exp->test.bench.max_dom = (double *) malloc(D* sizeof (double) );
    exp->test.bench.min_dom = (double *) malloc(D* sizeof (double) );
    
    for (i=0;i<D;i++) {
                exp->test.bench.max_dom[i] = Up;
                exp->test.bench.min_dom[i] = Lo;
    }
    *target = (*exp).test.tolerance;        
    *ftarget = 0;
    
    return 1;
}

void*  functiontest2(double *x,  void *data) {
       experiment_total *exp1;
       exp1 = (experiment_total *) data;
       //AMIGO_problem* amigo_problem;
       //amigo_problem = exp1[0].amigo;
       //int D;
        
    
	output_function *res;
        res = NULL;
        res = (output_function *) malloc(sizeof(output_function));
        

        res->g = (double *) malloc( (exp1[0].methodScatterSearch->ineq)  *sizeof(double));
        
        res->value=pow(x[1],2) + pow(x[2],2) + 2.0*pow(x[0],2) + pow(x[3],2) - 5.0*x[1] - 5.0*x[2] - 21.0*x[0] + 7.0*x[3];
        
        res->g[0] = pow(x[1],2) + pow(x[2],2) + pow(x[0],2) + pow(x[3],2) + x[1] - x[2] + x[0] - x[3];
        res->g[1] = pow(x[1],2) + 2.0*pow(x[2],2) + pow(x[0],2) + 2.0*pow(x[3],2) - x[1] - x[3];
        res->g[2] = 2.0*pow(x[1],2) + pow(x[2],2) + pow(x[0],2) + 2.0*x[1] - x[2] - x[3];

	return (void *)res;
}

void* evalSB_(double *U, void* data) {
    experiment_total *exp1;
    AMIGO_problem* amigo_problem;
    output_function *res;

    exp1 = (experiment_total *) data;
    amigo_problem = exp1[0].amigo;
    res = NULL;
    res = (output_function *) calloc(1,sizeof(output_function));

    set_AMIGO_problem_pars(U, amigo_problem);
    res->value = eval_AMIGO_problem_LSQ(amigo_problem);
    amigo_problem->nevals++;

    return ((void *) res);

}

double fgeneric_SB(experiment_total *exp, double tolerance) {
    double ftarget;
    ftarget = eval_AMIGO_problem_LSQ(exp->amigo);

    return ftarget + tolerance;
}



double DE_evaluation_NL2SOL(double *U, void* data, double *point, int *stop, void *array_stop, int *NPROC, int *D, int *neval) {
    experiment_total *exp1;
    exp1 = (experiment_total *) data;
    AMIGO_problem* amigo_problem;
    amigo_problem = exp1[0].amigo;
    double valor;
    
    
    set_AMIGO_problem_pars(U, amigo_problem);
    memmove(amigo_problem->x0,U,amigo_problem->nx*sizeof(double));


    if (exp1[0].test.bench.current_bench == 0 || exp1[0].test.bench.current_bench == 1) {
        printf("DIFERENCIAS -- %d\n", exp1[0].execution.idp);
        NL2SOL_AMIGO_pe(amigo_problem, 0, stop, array_stop, NPROC, neval);
    } else if (exp1[0].test.bench.current_bench == 2) {
        printf("GRADIENT -- %d\n", exp1[0].execution.idp);
        NL2SOL_AMIGO_pe(amigo_problem, 1, stop, array_stop, NPROC, neval);
    }

    memmove(point,amigo_problem->xbest,*D*sizeof(double));

    set_AMIGO_problem_pars(point, amigo_problem);
    point[*D] = eval_AMIGO_problem_LSQ(amigo_problem);
    valor = point[*D];
    amigo_problem->nevals++;

    return (valor);

}

const char* return_benchmark_SystemBiology(int i) {
    if (i == 0) {
        return "circadian";
    } else if (i == 1) {
        return "mendes";
    } else if (i == 2) {
        return "nfkb";
    } else if (i == 3) {
        return "logic";        
    } else {
        return "";
    }
}

void init_point_SystemBiology_Problems_matrix_(void *exp1_, double *popul, int NP, int D, int NPROC) {
    //experiment_total *exp1;
    //exp1 = (experiment_total *) exp1_;

    //openhdf5solutions_(exp1_, popul,&D,&NP);
    experiment_total *exp1;
    exp1 = (experiment_total *) exp1_;

    const char *name;
    const char *path1;
    const char *path2;
    const char *path3;
    const char *path4;
    const char *path5;
    const char *path6;
    int cc;

    path1 = "config/init_points/normal/matrix_circadian";
    path2 = "config/init_points/normal/matrix_mendes";
    path3 = "config/init_points/normal/matrix_nfkb";
    path4 = "config/init_points/log/matrix_circadian";
    path5 = "config/init_points/log/matrix_mendes";
    path6 = "config/init_points/log/matrix_nfkb";


    char const *coop;
    coop = "cooperative";
    if (exp1->par_st != NULL) {
        if (strcmp(exp1->par_st->type, coop) == 0) cc = 1;
        else cc = 0;
    } else cc = 0;


    name = return_benchmark_SystemBiology(exp1->test.bench.current_bench);

    if (strcmp(name, "circadian") == 0) {
        if (exp1->test._log == 1) {
            read_matrix_init_points(path4, popul, NP, D, cc, NPROC);
        } else read_matrix_init_points(path1, popul, NP, D, cc, NPROC);
    } else if (strcmp(name, "mendes") == 0) {
        if (exp1->test._log == 1) {
            read_matrix_init_points(path5, popul, NP, D, cc, NPROC);
        } else read_matrix_init_points(path2, popul, NP, D, cc, NPROC);
    } else if (strcmp(name, "nfkb") == 0) {
        if (exp1->test._log == 1) read_matrix_init_points(path6, popul, NP, D, cc, NPROC);
        else read_matrix_init_points(path3, popul, NP, D, cc, NPROC);
    }

}



int load_benchmark_SystemBiology(experiment_total *exp) {
    const char *path;
    int cont, i;
    int type;

    type = -1;

    
    if (exp->test.bench.current_bench == 0) {
        printf("LOAD CIRCADIAN\n");
        path = "config/circadian/load_C.mat";
        type = 0;
        (*exp).test.tolerance = 1e-5;
        

    } else if (exp->test.bench.current_bench == 1) {
        printf("LOAD MENDES\n");
        path = "config/mendes/load_C.mat";
        type = 1;
        (*exp).test.tolerance = 1e-5;
    } else if (exp->test.bench.current_bench == 2) {
        printf("LOAD NFKB\n");
        path = "config/nfkb/load_C.mat";
        type = 2;
        (*exp).test.tolerance = 3e-2;
    }else if (exp->test.bench.current_bench == 3) {
        printf("LOAD LOGIC\n");
        path = "config/LOGIC/load_C.mat";
        type = 3;
        (*exp).test.tolerance = 1e-3;
    } 
     else {
        path = "";
    }
    
    exp->param = NULL;
    exp->amigo = NULL;
    exp->amigo =  openMatFileAMIGO(path);
    
    
    //exp->amigo = (AMIGO_problem *) openMatFile((char *) path);       
    if (exp->amigo != NULL) {
        

        if (type == 0) {
            set_AMIGO_problem_rhs(exp->amigo, amigoRHS_CIRCADIAN, amigo_Y_at_tcon_CIRCADIAN);
            set_AMIGO_problem_obs_function(exp->amigo, amigoRHS_get_OBS_CIRCADIAN, amigoRHS_get_sens_OBS_CIRCADIAN);
        } else if (type == 1) {
            set_AMIGO_problem_rhs(exp->amigo, amigoRHS_MENDES, amigo_Y_at_tcon_MENDES);
            set_AMIGO_problem_obs_function(exp->amigo, amigoRHS_get_OBS_MENDES, amigoRHS_get_sens_OBS_MENDES);
        } else if (type == 2) {
            set_AMIGO_problem_rhs(exp->amigo, amigoRHS_NFKB, amigo_Y_at_tcon_NFKB);
            set_AMIGO_problem_obs_function(exp->amigo, amigoRHS_get_OBS_NFKB, amigoRHS_get_sens_OBS_NFKB);
        } else if (type == 3) {
            set_AMIGO_problem_rhs(exp->amigo, amigoRHS_LOGIC, amigo_Y_at_tcon_LOGIC);
            set_AMIGO_problem_obs_function(exp->amigo, amigoRHS_get_OBS_LOGIC, amigoRHS_get_sens_OBS_LOGIC);
        }        
        cont = exp->amigo->nx;
        
        exp->test.bench.dim = cont;
        exp->test.bench.BestSol = (double *) malloc( ( cont + 1) * sizeof (double));

        for (i = 0; i < exp->amigo->nx; i++) {
            exp->test.bench.BestSol[i] = exp->amigo->x[i];
        }

        exp->test.bench.BestSol[cont] = eval_AMIGO_problem_LSQ(exp->amigo);

        exp->test.bench.max_dom = (double *) malloc(   cont * sizeof (double));
        exp->test.bench.min_dom = (double *) malloc(   cont * sizeof (double));

        if ((*exp).test._log == 1) {
            exp->test.bench.log_max_dom = (double *) malloc( cont* sizeof (double));
            exp->test.bench.log_min_dom = (double *) malloc( cont* sizeof (double));
            
            for (i = 0; i < cont; i++) {
                exp->test.bench.max_dom[i] =exp->amigo->UB[i];
                exp->test.bench.min_dom[i] =exp->amigo->LB[i];
                        
                if (exp->amigo->LB[i] == 0) exp->test.bench.log_min_dom[i] = log(EPSILON_LOG);
                else exp->test.bench.log_min_dom[i] = log(exp->amigo->LB[i]);

                if (exp->amigo->UB[i] == 0) exp->test.bench.log_max_dom[i] = log(EPSILON_LOG);
                else exp->test.bench.log_max_dom[i] = log(exp->amigo->UB[i]);
            }
        } else {
            for (i = 0; i < cont; i++) {
                exp->test.bench.max_dom[i] = exp->amigo->UB[i];
                exp->test.bench.min_dom[i] = exp->amigo->LB[i];

            }
        }
        
         

        exp->amigo->cvodes_gradient = 1;
        exp->amigo->use_gradient = 1;
        exp->amigo->mkl_gradient = 0;
        
        
        return 1;

    } else return -1;
    
    


}


int destroySystemBiology(experiment_total *exp) {
    
    if (exp->amigo != NULL) {
        free_AMIGO_problem(exp->amigo);
        exp->amigo=NULL;
    }

    return 1;

}




void copylocalexperimentsb_(experiment_total *exp1, experiment_total *exparray, int NPROC) {
    
    int i;

        for (i = 0; i < NPROC; i++) {
        exparray[i].execution.NM = (*exp1).execution.NM;
        exparray[i].execution.NP = (*exp1).execution.NP;
        exparray[i].execution.NPROC = (*exp1).execution.NPROC;
        exparray[i].execution.contadorMigra = (*exp1).execution.contadorMigra;
        exparray[i].execution.enterMigrat = (*exp1).execution.enterMigrat;
        exparray[i].execution.ftarget = (*exp1).execution.ftarget;
        exparray[i].execution.idp = (*exp1).execution.idp;
        exparray[i].execution.initTIME = (*exp1).execution.initTIME;
        exparray[i].execution.maxfunevals = (*exp1).execution.maxfunevals;
        exparray[i].execution.migra_asin_wait = (*exp1).execution.migra_asin_wait;
        exparray[i].execution.migration = (*exp1).execution.migration;
        exparray[i].execution.minvarcondition = (*exp1).execution.minvarcondition;
        exparray[i].execution.nameMatlab = (*exp1).execution.nameMatlab;
        exparray[i].execution.num_it = (*exp1).execution.num_it;
        exparray[i].execution.send_id = (*exp1).execution.send_id;
        exparray[i].execution.size_send_buffer = (*exp1).execution.size_send_buffer;
        exparray[i].execution.st_sent = (*exp1).execution.st_sent;
        exparray[i].execution.stuckcond_lowVar = (*exp1).execution.stuckcond_lowVar;
        exparray[i].execution.stuckcount = (*exp1).execution.stuckcount;
        exparray[i].execution.tam = (*exp1).execution.tam;
        exparray[i].execution.topology = (*exp1).execution.topology;
        exparray[i].execution.update = (*exp1).execution.update;
        exparray[i].execution.rep = (*exp1).execution.rep;
	exparray[i].execution.initpath = (*exp1).execution.initpath;       
 
        exparray[i].test.maxtime = (*exp1).test.maxtime;
        exparray[i].test._log = (*exp1).test._log;
        exparray[i].test.init_point = (*exp1).test.init_point;
        exparray[i].test.init_repetition = (*exp1).test.init_repetition;
        exparray[i].test.local_search = (*exp1).test.local_search;
        exparray[i].test.max_eval = (*exp1).test.max_eval;
        exparray[i].test.namexml = (*exp1).test.namexml;
        exparray[i].test.ouput = (*exp1).test.ouput;
        exparray[i].test.output_graph = (*exp1).test.output_graph;
        exparray[i].test.repetitions = (*exp1).test.repetitions;
        exparray[i].test.tolerance = (*exp1).test.tolerance;
        exparray[i].test.type = (*exp1).test.type;
        exparray[i].test.local_gradient = (*exp1).test.local_gradient;
        exparray[i].test.output_temp = (*exp1).test.output_temp;
        exparray[i].test.verbose = (*exp1).test.verbose;
        
                        
        exparray[i].test.bench.min_dom = (double *) malloc(sizeof (double));
        exparray[i].test.bench.max_dom = (double *) malloc(sizeof (double));
        
        exparray[i].test.bench.dim = exp1[0].test.bench.dim;
        exparray[i].test.bench.type = (*exp1).test.bench.type;

        exparray[i].methodRandomSearch = exp1[0].methodRandomSearch;
        exparray[i].methodScatterSearch = exp1[0].methodScatterSearch;
        exparray[i].methodDE = exp1[0].methodDE;
        exparray[i].test.bench.current_bench = exp1[0].test.bench.current_bench;
        
        
        load_benchmark_SystemBiology(&(exparray[i]));
        
        if (exp1[0].methodDE != NULL) {
            exparray[i].methodDE = (experiment_method_DE *) malloc(sizeof (experiment_method_DE));
            exparray[i].methodDE->_NP = exp1[0].methodDE->_NP;
            exparray[i].methodDE->cache = exp1[0].methodDE->cache;
            exparray[i].methodDE->ls_counter = exp1[0].methodDE->ls_counter;
            exparray[i].methodDE->ls_threshold = exp1[0].methodDE->ls_threshold;
            exparray[i].methodDE->name = exp1[0].methodDE->name;
            exparray[i].methodDE->_F = exp1[0].methodDE->_F;
            exparray[i].methodDE->_CR = exp1[0].methodDE->_CR;
            exparray[i].methodDE->mutation_strategy = exp1[0].methodDE->mutation_strategy;
            exparray[i].methodDE->name = exp1[0].methodDE->name;
            
            
        } else {
            exparray[i].methodDE = NULL;
        }
        
        if (exp1[0].methodRandomSearch != NULL) {
            exparray[i].methodRandomSearch = (experiment_method_RandomSearch *) malloc(sizeof (experiment_method_RandomSearch));
            exparray[i].methodRandomSearch->_NP = exp1[0].methodRandomSearch->_NP;
            exparray[i].methodRandomSearch->cache = exp1[0].methodRandomSearch->cache;
            exparray[i].methodRandomSearch->ls_counter = exp1[0].methodRandomSearch->ls_counter;
            exparray[i].methodRandomSearch->ls_threshold = exp1[0].methodRandomSearch->ls_threshold;
            exparray[i].methodRandomSearch->name = exp1[0].methodRandomSearch->name;
        } else {
            exparray[i].methodRandomSearch = NULL;
        }
        
        if (exp1[0].methodScatterSearch != NULL) {
            exparray[i].methodScatterSearch = (experiment_method_ScatterSearch*) malloc(sizeof (experiment_method_ScatterSearch));
            exparray[i].methodScatterSearch->_NP = exp1[0].methodScatterSearch->_NP;
            exparray[i].methodScatterSearch->bin_var = exp1[0].methodScatterSearch->bin_var;
            exparray[i].methodScatterSearch->cache = exp1[0].methodScatterSearch->cache;
            exparray[i].methodScatterSearch->goptions = (global_options *) malloc(sizeof(global_options)); 
            exparray[i].methodScatterSearch->goptions = exp1[0].methodScatterSearch->goptions;
            exparray[i].methodScatterSearch->loptions = (local_options *) malloc(sizeof(local_options)); 
            exparray[i].methodScatterSearch->loptions = exp1[0].methodScatterSearch->loptions;
            exparray[i].methodScatterSearch->uoptions = (user_options *) malloc(sizeof(user_options)); 
            exparray[i].methodScatterSearch->uoptions = exp1[0].methodScatterSearch->uoptions;
            
            exparray[i].methodScatterSearch->index_log = (int *) malloc( exp1[0].test.bench.dim * sizeof(int) );
            exparray[i].methodScatterSearch->ineq = exp1[0].methodScatterSearch->ineq;
            exparray[i].methodScatterSearch->int_var = exp1[0].methodScatterSearch->int_var;
            exparray[i].methodScatterSearch->ls_counter = exp1[0].methodScatterSearch->ls_counter;
            exparray[i].methodScatterSearch->ls_threshold = exp1[0].methodScatterSearch->ls_threshold;
            exparray[i].methodScatterSearch->name = exp1[0].methodScatterSearch->name;
        } else {
            exparray[i].methodScatterSearch = NULL;
        }   
        
        if (exp1[0].par_st != NULL) {
            exparray[i].par_st = (parallelization_strategy *) malloc(sizeof (parallelization_strategy));
            exparray[i].par_st->NPROC = exp1[0].par_st->NPROC;
            exparray[i].par_st->ReplacePolicy = exp1[0].par_st->ReplacePolicy;
            exparray[i].par_st->SelectionPolicy = exp1[0].par_st->SelectionPolicy;
            exparray[i].par_st->commu = exp1[0].par_st->commu;
            exparray[i].par_st->islandstrategy = exp1[0].par_st->islandstrategy;
            exparray[i].par_st->migration_freq_ite = exp1[0].par_st->migration_freq_ite;
            exparray[i].par_st->migration_size = exp1[0].par_st->migration_size;
            exparray[i].par_st->topology = exp1[0].par_st->topology;
            exparray[i].par_st->type = exp1[0].par_st->type;
        } else {
            exparray[i].par_st = NULL;
        }
        
        if (exp1[0].param != NULL) {
            exparray[i].param = (ParamStruct *) malloc(sizeof (ParamStruct));
        } else {
            exparray[i].param = NULL;
        }
        
        
        
    }
    
    

}
